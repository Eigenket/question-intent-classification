# Question intent classification

[[_TOC_]]

## Build an unsupervised model to cluster form questions (main.py)

When you are working with raw text data you must assume that not all of the text will be useful to extract features from. NLTK helps us to extract useful information.

### NLTK 

NLTK allows you to use some of the more basic NLP functionalities. Specifically we are going to use the following functions
- nltk.tokenize.word_tokenize
- nltk.tokenize.RegexpTokenizer

### gensim

Gensim is a machine learning library that is heavily focused on applying machine learning and deep learning to NLP tasks.
In our case we are interested in Word2Vec to use tha Skip-Gram model

#### Word2Vec
Tomas Mikolov, Ilya Sutskever, Kai Chen, Greg Corrado, and Jeffrey Dean are credited with creating Word2Vec in 2014 while working at Google.

Word2Vec provides a method for finding vector representations of words and phrases, and it can be expanded as much as the documents. The Word2Vec model has two main models, namely, CBOW and Skip-Gram models. 

```mermaid
graph LR
    subgraph a["CBOW MODEL"]
    INPUT([INPUT]) --> PROJECTION([PROJECTION]) --> OUTPUT([OUTPUT])
    
    A["w(t-2)"] & B["w(t-1)"] & C[" "] & D["w(t+1)"] & E["w(t+2)"] --> SUM[" "] --> F["w(t)"]
    end
```

CBOW model is a shallow neural network whose objective is to predicting the words related to the context.

```mermaid
graph LR
    subgraph a["Skip-Gram MODEL"]
    INPUT([INPUT]) --> PROJECTION([PROJECTION]) --> OUTPUT([OUTPUT])
    
     F["w(t)"] --> SUM[" "] --> A["w(t-2)"] & B["w(t-1)"] & C[" "] & D["w(t+1)"] & E["w(t+2)"]
    end
```

Skip-Gram model is a shallow neural network whose objective is to predict the context from the words.



## Steps description of the code

First of all we are going to define the processes in main.py:

- Tokenizer questions and removal of stop words : Each question is splitted in words and words defined as very common words in a
given language, called stop words, are removed. Example stop words include the, and, of, ...

- Creation of Word2Vec model: We are going to consider Skip-Gram model 

- Question Embeddings: Mapping questions to vector space to obtain question_embedding

- Kmeans cluster Algorithm: silouette_score is used to find the optimal number of clusters

- Hierarchical cluster Algorithm: silouette_score is used to find the optimal number of clusters (Can be also visually determined bydendrograms)

- Plotting Clusters in 2D by means of PCA dimensionality reduction

## Results, assumptions and modelling decisions

We selected Skip-Gram model because CBOW has the disadvantage that takes the average of the context of a word. By contrast Skip-Gram model can capture semantics for a single word.

<img src="img/kmeans.png" alt="API_web"
	title="API_web" width="420" height="400" />
<img src="img/hierarchical.png" alt="API_web"
	title="API_web" width="420" height="400" />