import pandas as pd
import numpy as np
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

from sklearn.cluster import AgglomerativeClustering
import matplotlib.pyplot as plt

def tokenizer_questions(question):
    sample_word_tokens = word_tokenize(question)
    # Tokenize
    tokenizer = RegexpTokenizer(r'\w+')
    sample_word_tokens = tokenizer.tokenize(str(sample_word_tokens))
    sample_word_tokens = [word.lower() for word in sample_word_tokens]
    stop_words = set(stopwords.words('english'))
    sample_word_tokens = [w for w in sample_word_tokens if not w in stop_words]
    if len(sample_word_tokens) == 0:
        return None
    return sample_word_tokens

def vectorizer(question, skip_gram):
    vec = []
    numw = 0
    for w in question:
        try:
            if numw == 0:
                vec=skip_gram[w]
            else:
                vec=np.add(vec, skip_gram[w])
            numw+=1
        except:
            pass
    return np.asarray(vec) / numw

def optimal_number_of_clusters(question_embedding):
    max_num_clusters = 8
    # silouette_score method for determining optimal number of clusters
    silhouette_score_list = []
    for i in range(2, max_num_clusters+1):
        kmeans = KMeans(n_clusters = i, random_state = 42)
        kmeans.fit(question_embedding)
        labels = kmeans.labels_
        silhouette_score_list.append(silhouette_score(question_embedding, labels, metric='euclidean'))
    df_silhouette = pd.DataFrame({'silhouette_score':silhouette_score_list})
    df_silhouette['clusters'] = range(2, max_num_clusters+1)
    df_silhouette['silhouette_score_diff'] = df_silhouette['silhouette_score'].diff()
    df_silhouette = df_silhouette[df_silhouette['silhouette_score_diff'] > 0]
    num_clusters = df_silhouette['clusters'][df_silhouette['clusters'] == df_silhouette['clusters'].min()].iloc[0]
    return num_clusters

def optimal_number_of_clusters_hc(question_embedding):
    max_num_clusters = 8
    # silouette_score method for determining optimal number of clusters
    silhouette_score_list = []
    for i in range(2, max_num_clusters+1):
        hc = AgglomerativeClustering(n_clusters = i, affinity = 'euclidean', linkage='ward')
        labels = hc.fit_predict(question_embedding)
        silhouette_score_list.append(silhouette_score(question_embedding, labels, metric='euclidean'))
    df_silhouette = pd.DataFrame({'silhouette_score':silhouette_score_list})
    df_silhouette['clusters'] = range(2, max_num_clusters+1)
    df_silhouette['silhouette_score_diff'] = df_silhouette['silhouette_score'].diff()
    df_silhouette = df_silhouette[df_silhouette['silhouette_score_diff'] > 0]
    num_clusters = df_silhouette['clusters'][df_silhouette['clusters'] == df_silhouette['clusters'].min()].iloc[0]
    return num_clusters


if __name__ == '__main__':
    # Loading data
    df = pd.read_csv("form_questions.csv")
    df = df.dropna()

    # Tokenizer questions and removal of stop words
    df['word_tokens'] = df['question'].apply(lambda x: tokenizer_questions(x))
    df = df.dropna()
    
    # Creation of Word2Vec model with Skip-Gram Model
    tokenized_questions = list(df['word_tokens'].values)
    skip_gram = Word2Vec(tokenized_questions, size=100, window=1, min_count=1, sg=1)

    # Question Embeddings
    question_embedding = []
    for question in tokenized_questions:
        vector_question = vectorizer(question, skip_gram)
        question_embedding.append(vector_question)
    question_embedding=np.array(question_embedding)

    # Clustering Kmeans
    num_clusters = optimal_number_of_clusters(question_embedding)

    kmeans = KMeans(n_clusters = num_clusters, random_state = 42)
    k_labels = kmeans.fit_predict(question_embedding)

    # Plotting
    pca = PCA(n_components=2)
    question_embedding_pca = pca.fit_transform(question_embedding)
    label_colors = ['#d62d20', '#800080', '#0057e7', '#ffa700', '#008744', '#2F4F4F', '#FF1493']
    
    colors = [label_colors[i] for i in k_labels]
    plt.scatter(question_embedding_pca[:,0], question_embedding_pca[:,1], c=colors)
    plt.title("KMeans Number of Clusters " + str(num_clusters))
    plt.show()

    # Hierarchical Clusters
    num_clusters = optimal_number_of_clusters_hc(question_embedding)
    hc = AgglomerativeClustering(n_clusters = num_clusters, affinity = 'euclidean', linkage='ward')
    hc_labels = hc.fit_predict(question_embedding)

    # Plotting
    colors = [label_colors[i] for i in hc_labels]
    plt.scatter(question_embedding_pca[:,0], question_embedding_pca[:,1], c=colors)
    plt.title("Hierarchical Clustering Number of Clusters " + str(num_clusters))
    plt.show()